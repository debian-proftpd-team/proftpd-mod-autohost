proftpd-mod_autohost
====================

Status
------
[![Build Status](https://travis-ci.org/Castaglia/proftpd-mod_autohost.svg?branch=master)](https://travis-ci.org/Castaglia/proftpd-mod_autohost)

Synopsis
--------
The `mod_autohost` module for ProFTPD is used to support large numbers
of `<VirtualHost>` sections _without_ requiring large config files to do so.

For further module documentation, see [mod_autohost.html](https://htmlpreview.github.io/?https://github.com/Castaglia/proftpd-mod_autohost/blob/master/mod_autohost.html).
